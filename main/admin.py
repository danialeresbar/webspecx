from django.contrib import admin
from main.models import Audit, Configuration


class AuditAdmin(admin.ModelAdmin):
    readonly_fields = ('creation_date', 'update_date')


@admin.register(Configuration)
class ConfigurationAdmin(admin.ModelAdmin):
    list_display = ["key", "value"]
