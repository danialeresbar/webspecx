from django.db import models


class Audit(models.Model):
    """

    """

    creation_date = models.DateTimeField(verbose_name='Created', auto_now_add=True, null=True)
    update_date = models.DateTimeField(verbose_name='Last modified', auto_now=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        abstract = True


class Configuration(models.Model):
    key = models.CharField(max_length=200)
    value = models.CharField(max_length=200)

    def __str__(self):
        return self.key
