# webspecx

Django 3.1.1 + Postgres 12 + Dokku config (Production Ready)

## Documentation ##

Specx es un aplicativo que permite la configuración de escenarios adecuados para la generación de 
variables aleatorias cada cierto tiempo de acuerdo con ciertas distribuciones de probabilidad 
identificadas en un proceso de muestreo realizado a las frecuencias de televisión digital en la 
ciudad de Villavicencio. El software realiza una la simulación de un proceso estocástico de tiempo 
sincrónico. La aplicación cuenta con las siguientes funcionalidades:

1. Asignar a cada frecuencia entendida como una variable aleatoria, alguna de las distribuciones de 
probabilidad tratadas anteriormente, y configurar los parámetros necesarios de esta.

2. Guardar y cargar archivos con las configuraciones realizadas para las distribuciones, con el fin 
de no repetir procesos. Los datos se almacenan en archivos con formato JSON.

3. Fijar un tiempo de muestreo en minutos.

4. La simulación solo se podrá iniciar en cuanto cada frecuencia tenga asignada y configurada una 
distribución de probabilidad.

5. Pausar y reanudar la simulación en cualquier momento.

6. Manipular el flujo del tiempo, lo que implica una escalización al tiempo de muestreo durante el 
proceso de simulación.

7. Visualizar la secuencia de valores generados para cada frecuencia en una gráfica por individual que 
se actualiza en tiempo real.

8. Guardar las gráficas construidas durante la simulación en formato PNG.

9. Al final de la simulación, el programa genera un archivo de texto plano con información de los valores 
generados.

El objetivo de *webspecx* es reforzar la información sobre los espacios en blanco de televisión que 
contienen las bases de datos como las que usa el protocolo [PAWS](https://tools.ietf.org/html/rfc7545). 
Información que brinda grandes oportunidades para un uso eficiente del espectro.

## Autores ✒️

_El equipo de trabajo que llevó a cabo el estudio de simulación y el desarrollo de *webspecx* está formado 
por las siguientes personas:_

* **Héctor Iván Reyes Moncayo** - *Formulación del problema y consideraciones al modelo* - [Director]()
* **Ángel Alfonso Cruz Roa** - *Modelo del sistema* - [Codirector]()
* **Daniel Alejandro Restrepo Barbosa** - *Proceso de muestreo y determinación de distribuciones de probabilidad, 
  diseño y construcción de interfaces gráficas de usuario.* - [Analista]()
* **Siervo Francisco Rodríguez Castellanos** - *Codificación de algoritmos de simulación y optimización 
  de rutinas* - [Desarrollador]()

### Directory Tree ###
```

├── main (Main application of the project, use it to add main templates, statics and root routes)
│   ├── fixtures
│   │   ├── dev.json (Useful dev fixtures, by default it creates an `admin` user with password `admin`)
│   │   └── initial.json (Initial fixture loaded on each startup of the project)
│   ├── migrations
│   ├── static (Add here the main statics of the app)
│   ├── templates (Add here the main templates of the app)
│   ├── admin.py
│   ├── apps.py
│   ├── models.py (Main models like City, Config)
│   ├── tests.py (We hope you will put some tests here :D)
│   ├── urls.py (Main urls, place the home page here)
│   └── views.py
├── media
├── webspecx
│   ├── settings
│   │   ├── partials
│   │   │   └── util.py (Useful functions to be used in settings)
│   │   ├── common.py (Common settings for different environments)
│   │   ├── development.py (Settings for the development environment)
│   │   └── production.py (Settings for production)
│   ├── urls.py
│   └── wsgi.py
├── scripts
│   ├── command-dev.sh (Commands executed after the development containers are ready)
│   └── wait-for-it.sh (Dev script to wait for the database to be ready before starting the django app)
├── static
├── Dockerfile (Instructions to create the project image with docker)
├── Makefile (Useful commands)
├── Procfile (Dokku or Heroku file with startup command)
├── README.md (This file)
├── app.json (Dokku deployment configuration)
├── docker-compose.yml (Config to easily deploy the project in development with docker)
├── manage.py (Utility to run most django commands)
└── requirements.txt (Python dependencies to be installed)
```

### How to run the project ###

The project use docker, so just run:

```
docker-compose up
```

> If it's first time, the images will be created. Sometimes the project doesn't run at first time because the init of postgres, just run again `docker-compose up` and it will work.

*Your app will run in url `localhost:8009`*

To recreate the docker images after dependencies changes run:

```
docker-compose up --build
```

To remove the docker containers including database (Useful sometimes when dealing with migrations):

```
docker-compose down
```

### Accessing Administration

The django admin site of the project can be accessed at `localhost:8009/admin`

By default the development configuration creates a superuser with the following credentials:

```
Username: admin
Password: admin
```

## Production Deployment: ##

The project is dokku ready, this are the steps to deploy it in your dokku server:

#### Server Side: ####

> This docs does not cover dokku setup, you should already have configured the initial dokku config including ssh keys

Create app and configure postgres:
```
dokku apps:create webspecx
dokku postgres:create webspecx
dokku postgres:link webspecx webspecx
```

> If you don't have dokku postgres installed, run this before:
> `sudo dokku plugin:install https://github.com/dokku/dokku-postgres.git`

Create the required environment variables:
```
dokku config:set webspecx ENVIRONMENT=production DJANGO_SECRET_KEY=....
```

Current required environment variables are:

* ENVIRONMENT
* DJANGO_SECRET_KEY
* EMAIL_PASSWORD

Use the same command to configure secret credentials for the app

#### Local Side: ####

Configure the dokku remote:

```
git remote add production dokku@<my-dokku-server.com>:webspecx
```

Push your changes and just wait for the magic to happens :D:

```
git push production master
```

Optional: To add SSL to the app check:
https://github.com/dokku/dokku-letsencrypt

Optional: Additional nginx configuration (like client_max_body_size) should be placed server side in:
```
/home/dokku/<app>/nginx.conf.d/<app>.conf
```

> Further dokku configuration can be found here: http://dokku.viewdocs.io/dokku/

## Expresiones de Gratitud 🎁

- A los directores de este proyecto. El doctor Ángel Cruz por sus aportes tan valiosos al modelo del 
  sistema, sin el cual Specx no podría operar correctamente; además de la dedicación a la revisión de 
  cada funcionalidad del software. El doctor Héctor Reyes por sus conocimientos en el área de 
  telecomunicaciones y radiotransmisiones, ya que su idea de un radio tvws fue la que llevó al desarrollo 
  de *webspecx*.

- A Francisco Rodríguez por sus habilidades en el desarrollo, que permitieron la construcción de varios 
  generadores de variables aleaotorias, así como la optimización en el uso de recursos por parte de la 
  aplicación.

- A la universidad de los llanos por brindar sus espacios y material bibliográfico para el estudio de 
  simulación realizado.
