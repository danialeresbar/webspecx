from django.apps import AppConfig


class SpectrumsConfig(AppConfig):
    name = 'spectrums'
