from django.db import models

from django_countries.fields import CountryField
from main.models import Audit


class Parameter(Audit):
    """

    """

    name = models.CharField(max_length=128)
    description = models.TextField(blank=True)

    class Meta:
        verbose_name = 'Parameter'
        verbose_name_plural = 'Parameters'

    def __str__(self):
        return self.name


class ProbabilityDistribution(Audit):
    """

    """

    TYPE_CHOICES = (
        ('CONTINUOUS', 'CONTINUOUS'),
        ('DISCRETE', 'DISCRETE')
    )

    name = models.CharField(max_length=64)
    description = models.TextField(blank=True)
    type = models.CharField(choices=TYPE_CHOICES, max_length=16)
    parameters = models.ManyToManyField(Parameter, through='DistributionParameter')

    class Meta:
        verbose_name = 'Probability Distribution'
        verbose_name_plural = 'Probability Distributions'

    def __str__(self):
        return self.name


class DistributionParameter(models.Model):
    distribution = models.ForeignKey(ProbabilityDistribution, on_delete=models.CASCADE)
    parameter = models.ForeignKey(Parameter, on_delete=models.CASCADE)
    value = models.DecimalField(max_digits=15, decimal_places=10)
    is_required = models.BooleanField(default=True)

    class Meta:
        unique_together = ['distribution', 'parameter']


class RadioBand(models.Model):
    """

    """

    name = models.CharField(max_length=64)
    designation = models.CharField(max_length=3)
    description = models.TextField(blank=True)
    start_frequency = models.DecimalField(max_digits=4, decimal_places=3)
    end_frequency = models.DecimalField(max_digits=4, decimal_places=2)


    class Meta:
        verbose_name = 'Radio Band'
        verbose_name_plural = 'Radio Bands'

    def __str__(self):
        return f'{self.designation}'


class ChannelFrequency(Audit):
    """

    """

    frequency = models.DecimalField(max_digits=4, decimal_places=1)
    band = models.ForeignKey(RadioBand, on_delete=models.CASCADE)
    probability_distribution = models.ForeignKey(ProbabilityDistribution, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Channel Frequency'
        verbose_name_plural = 'Channel Frequencies'

    def __str__(self):
        return f'{self.value} MHz'


class Channel(Audit):
    """

    """

    country = CountryField(blank_label='Select country')
    code = models.PositiveIntegerField(default=0, blank=True)
    frequency = models.ForeignKey(ChannelFrequency, on_delete=models.CASCADE)
    # TODO: Add coverage field

    class Meta:
        verbose_name = 'Channel'
        verbose_name_plural = 'Channels'

    def __str__(self):
        return f'{self.code}'
