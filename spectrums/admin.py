from django.contrib import admin

from main.admin import AuditAdmin
from spectrums.models import Channel, ChannelFrequency, DistributionParameter, Parameter, ProbabilityDistribution, RadioBand


@admin.register(Parameter)
class ParameterAdmin(admin.ModelAdmin):
    readonly_fields = AuditAdmin.readonly_fields
    fieldsets = (
        (None, {
            'fields': (AuditAdmin.readonly_fields,)
        }),
        ('Parameter', {
            'fields': ('name', 'description')
        })
    )
    list_display = ('id', 'name', 'creation_date')
    list_editable = ('name',)
    list_filter = ('creation_date',)


class DistributionParameterStackedInline(admin.StackedInline):
    model = DistributionParameter
    extra = 1


@admin.register(ProbabilityDistribution)
class ProbabilityDistributionAdmin(admin.ModelAdmin):
    readonly_fields = AuditAdmin.readonly_fields
    fieldsets = (
        (None, {
            'fields': (AuditAdmin.readonly_fields,)
        }),
        ('Distribution', {
            'fields': (('name', 'type'), 'description', )
        })
    )
    inlines = [DistributionParameterStackedInline]


@admin.register(RadioBand)
class RadioBandAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': (('name', 'designation'), 'description')
        }),
        ('Frequency Range', {
            'classes': ('collapse',),
            'fields': (('start_frequency', 'end_frequency'))
        })
    )
    list_display = ('name', 'designation', 'start_frequency', 'end_frequency')

    # TODO: Use this when we upload all the bands
    # def has_add_permission(self, request):
    #     return False
    #
    # def has_change_permission(self, request, obj=None):
    #     return False
    #
    # def has_delete_permission(self, request, obj=None):
    #     return False


@admin.register(ChannelFrequency)
class ChannelFrequencyAdmin(admin.ModelAdmin):
    readonly_fields = AuditAdmin.readonly_fields
    fieldsets = (
        (None, {
            'fields': (AuditAdmin.readonly_fields,)
        }),
        ('Frequency', {
            'fields': (('frequency', 'band'), 'probability_distribution')
        })
    )
    list_display = ('frequency', 'band', 'creation_date')
    list_filter = ('band', 'probability_distribution', 'creation_date')


@admin.register(Channel)
class ChannelAdmin(admin.ModelAdmin):
    readonly_fields = AuditAdmin.readonly_fields
    fieldsets = (
        (None, {
            'fields': (AuditAdmin.readonly_fields,)
        }),
        ('Channel', {
            'fields': ('country', 'code', 'frequency')
        })
    )

