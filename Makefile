superuser:
	docker exec -it webspecx ./manage.py createsuperuser

shell:
	docker exec -it webspecx ./manage.py shell

makemigrations:
	docker exec -it webspecx ./manage.py makemigrations

migrate:
	docker exec -it webspecx ./manage.py migrate

initialfixture:
	docker exec -it webspecx ./manage.py loaddata initial

testfixture:
	docker exec -it webspecx ./manage.py loaddata test

test:
	docker exec -it webspecx ./manage.py test

statics:
	docker exec -it webspecx ./manage.py collectstatic --noinput

makemessages:
	docker exec -it webspecx django-admin makemessages

compilemessages:
	docker exec -it webspecx django-admin compilemessages
